﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour {

    public Text TimTXT;
    float timer_Casa;
    float Timer_Afuera;
    float minutes;
    float seconds;
    // Use this for initialization
    void Start ()
    {
        timer_Casa = 5;
	
	}
	
	// Update is called once per frame
	void Update ()
    {
       
        timer_Casa -= Time.deltaTime;
        seconds = timer_Casa % 60;
        minutes = Mathf.Floor(timer_Casa / 60);

        TimTXT.text = "TIMER" +" =  " +"MIN:  "+ minutes + ":" + (int)seconds;

    }
}
