﻿using UnityEngine;
using System.Collections;
using Pathfinding;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]
public class EnemyAI : MonoBehaviour {

    public Transform Target;
    public Transform RetunrPoint;
    FieldOfView FieldTar;//mod
    Vector3 Current_Pos;//a
    Vector3 Last_Pos;//a


    SpriteRenderer Spr;

    float angle;

    Rigidbody2D body;

    public float updateRate = 2f;
    float lockPos = 0;//a

    private Seeker seeker;
    private Rigidbody2D Rb;

    public Path path;
    public float speed = 300;
    public float TurnSpeed = 5f;//a
    public ForceMode2D fmode;

    Vector3 _dir;//a



    [HideInInspector]
    public bool PathisEnded = false;

    public float nextWaypointDistance = 3;

    private int CurrentWaypoint = 0;

    private bool searchingForPlayer = false;
    void Start()
    {
        Spr = GetComponent<SpriteRenderer>();
        seeker = GetComponent<Seeker>();
        Rb = GetComponent<Rigidbody2D>();
        FieldTar = GetComponent<FieldOfView>();
       

        if(Target == null)
        {
           if(!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(searchgForPlayer());
            }
            return;
        }
        seeker.StartPath(transform.position, Target.position, OnPathCpmplete);
        //Countinue
        StartCoroutine(Update_path());
    }

    IEnumerator searchgForPlayer()
    {
        GameObject sResult = GameObject.FindGameObjectWithTag("Player");
        if(sResult == null)
        {
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(searchgForPlayer());
        }
        else
        {
            Target = sResult.transform;// original
           // Target = FieldTar.visibleTargets[0].transform;//MOD
            searchingForPlayer = false;
            StartCoroutine(Update_path());
           yield return false;
        }

        yield return new WaitForEndOfFrame();
    }

    IEnumerator Update_path()
    {
        // para busqueda
        if (Target == null)
        {
            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(searchgForPlayer());
            }
           yield return false;
        }
        seeker.StartPath(transform.position, Target.position, OnPathCpmplete);

        yield return new WaitForSeconds(1f/updateRate);
        StartCoroutine(Update_path());
    }


   public void OnPathCpmplete (Path p)
    {

        //cuando no se encuentre jugador
        Debug.Log("Path Found?" + p.error) ;
        if (!p.error)
        {
            path = p;
            CurrentWaypoint = 0;
        }
    }

    void LookAtTarget()
    {

        body = GetComponent<Rigidbody2D>();


        Vector3 player = new Vector3(Target.position.x, Target.position.y);

        float AngleRad = Mathf.Atan2(player.y - gameObject.transform.position.y, player.x - transform.position.x);
        float angle = (180 / Mathf.PI) * AngleRad;
      //   angle = angle- 180;

        body.rotation = angle;

    }

    void FixedUpdate()
    {

        if (Target == null)
        {
            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(searchgForPlayer());
            }
            return;
        }

        //Sistema de busqueda, simpre suiendoo al jugador?
        if (path == null)
        {
            return;
        }

        if (CurrentWaypoint >= path.vectorPath.Count)//Expirementar; para que la AI deje de perseguir despues de cierta distancia
        {
            if (PathisEnded)
            {
                return;
            }
            Debug.Log("End of the path Reach");
            PathisEnded = true;
            return;
        }
        PathisEnded = false;

        Vector3 Dir = (path.vectorPath[CurrentWaypoint] - transform.position).normalized;

        Dir *= speed * Time.fixedDeltaTime;

        //Move AI

        Rb.AddForce(Dir, fmode);

        float dist = Vector3.Distance(transform.position, path.vectorPath[CurrentWaypoint]);

        if (dist < nextWaypointDistance)
        {
            CurrentWaypoint++;
            return;
        }
    }
    IEnumerator Change()
    {

        if (Vector3.Distance(transform.position, Target.position)> FieldTar.viewRadius)
        {
            Target = RetunrPoint;

            Debug.Log("DISTANCE : " +Vector3.Distance(transform.position, Target.position));
          

        }

        yield return new WaitForSeconds(1f);
       // Target = RetunrPoint;
    }
    void Update()
    {
        LookAtTarget();
        StartCoroutine(Change());
        Target = FieldTar.visibleTargets[0].transform;

        if(gameObject.transform.rotation.z>170 && gameObject.transform.rotation.z < 190)
        {
            Spr.flipY = true;
        }

        //   LookPlayer();
        // Current_Pos = gameObject.transform.position;//a
        //  transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, lockPos, lockPos);//a
        //    Last_Pos = Current_Pos- //a
        //   DoStuff(Current_Pos, Last_Pos);//


    }

}
