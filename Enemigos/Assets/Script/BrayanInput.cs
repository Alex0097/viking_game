﻿using UnityEngine;
using System.Collections;

public class BrayanInput : MonoBehaviour {

    //=========================================================
    public float BrayanSpeed;
    
    public Transform grundCheckPoint;
    public LayerMask whatisGround;
    public bool ImInGround;

    

    public Transform AtackPoint;

    public LayerMask whatatack;

    public GameObject moneyExplosion;

    public int damage;


    private Rigidbody2D Brayanbody;

    private Vector2 BrayanMovement;

    private float horImput;

    private Transform BrayanTarget;



    private Animator anim;

    private bool brayanMirandoParaDondeEs;

    private float time;

    private float timeAtack;

    //=======================================================
    void Start()
    {
        this.Brayanbody = this.GetComponent<Rigidbody2D>();
        this.BrayanMovement = new Vector2();
        this.ImInGround = false;

        this.anim = GetComponent<Animator>();

        this.brayanMirandoParaDondeEs = true;

        this.time = 0;
        this.timeAtack = 1;

        GameObject tmp = GameObject.FindGameObjectWithTag("Player");
        if(tmp != null)
        {
            this.BrayanTarget = tmp.transform;

        }

    }
    //====================================================

    void Update()
    {


        this.time += Time.deltaTime;

        if(this.time > this.timeAtack)
        {
            this.time = 0;
            this.Atack();

        }
        if (this.BrayanTarget) { 
        if(this.transform.position.x < this.BrayanTarget.position.x){

            this.horImput = 1;
            
        }else if (this.transform.position.x > this.BrayanTarget.position.x)
        {

            this.horImput = -1;
            }
        }
        if ((this.horImput < 0) && (this.brayanMirandoParaDondeEs))
        {
            this.Flip();
            this.brayanMirandoParaDondeEs = false;

        }
        else if ((this.horImput > 0) && (!this.brayanMirandoParaDondeEs))
        {
            this.Flip();
            this.brayanMirandoParaDondeEs = true;

        }



        this.anim.SetFloat("VerSpeed", Mathf.Abs(this.Brayanbody.velocity.y));

        if (Physics2D.OverlapCircle(this.grundCheckPoint.position, 0.10f, this.whatisGround))
        {

            this.ImInGround = true;

        }
        else
        {
            this.ImInGround = false;

        }

    }
    //========================================================
    void FixedUpdate()
    {
        this.BrayanMovement = this.Brayanbody.velocity;

        this.BrayanMovement.x = horImput * BrayanSpeed;
       

        this.Brayanbody.velocity = this.BrayanMovement;


    }
    //========================================================
    void Flip()
    {
        Vector3 scale = this.transform.localScale;
        scale.x *= (-1);
        this.transform.localScale = scale;

    }
    //====================================================

    void Atack()
    {
        Collider2D tmp = Physics2D.OverlapCircle(this.AtackPoint.position, 0.10f, this.whatatack);


        if (tmp)
        {
            tmp.gameObject.SendMessage("Hurt", this.damage);

        }
        

    }

}

