﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Hearth : MonoBehaviour {

    public Slider hearthBar;

    public float Hp;

    public GameObject moneyExplosion;

    //==============================
    void Start () {
	
	}
	
	//================================
	void Update ()
    {
        if (Input.GetKey(KeyCode.Y))
        {
            Hurt(2);
        }
	
	}

    public void Hurt(float damage)
    {
      

        if(this.Hp <= 0)
        {
            this.Hp = 0;
          GameObject be = Instantiate(this.moneyExplosion, this.transform.position, Quaternion.identity) as GameObject;

            Destroy(be, 0.5f);

            this.gameObject.SendMessage("OnGetKill");

            Destroy(this.gameObject);

        }
        this.Hp -= damage;
        if (this.hearthBar)
        {
            this.hearthBar.value = this.Hp;

        }

    }

    
}
