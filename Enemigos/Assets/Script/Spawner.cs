﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

    //===============================

    public GameObject BrayanPrefap;
    public int Brayanlimit;


    private float time;
    private float timeToSpwaner;
    private float BrayansCount;

	//===============================
	void Start () {
        this.time = 0;
        
        this.timeToSpwaner = 5;
	
	}
    //================================
    void OnEnable()
    {
        this.BrayansCount = 0;

    }
	
	//=================================
	void Update () {

        this.time += Time.deltaTime;
        if(this.time > this.timeToSpwaner)
        {

            this.time = 0;
            Vector3 BrayanPrefabPosition = new Vector3(this.transform.position.x ,this.transform.position.y ,1);
            Instantiate(this.BrayanPrefap, BrayanPrefabPosition, Quaternion.identity);
            
            this.BrayansCount++;
            if(this.BrayansCount >= Brayanlimit)
            {

                this.gameObject.SetActive(false);
            }

        }

	}
}
