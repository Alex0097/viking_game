﻿using UnityEngine;
using System.Collections;

public class BulletMovement : MonoBehaviour {

    public GameObject player;
    private Transform playerTrans;

    public Rigidbody2D bulletRB;


    public float bulletSpeed;


    public float bulletLife;

    public static int damage;
    public int damageRef;

   

    //todo metodo awake inicia antes del star

    void Awake()
    {
        damage = damageRef;

        bulletRB = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerTrans = player.transform;
    }
    // Use this for initialization
    void Start()
    {


        if (playerTrans.localScale.x > 0)
        {
            bulletRB.velocity = new Vector2(bulletSpeed, bulletRB.velocity.x);
            transform.localScale = new Vector3(1, 1, 1);

        }
        else
        {

            bulletRB.velocity = new Vector2(-bulletSpeed, bulletRB.velocity.y);
          
        }
    }

    // Update is called once per frame
    void Update()
    {

        Destroy(gameObject, bulletLife);

    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Plartfom" || col.tag == "Enemy")
        {

           
            
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<BoxCollider2D>().enabled = false;

        }

    }
}

 
