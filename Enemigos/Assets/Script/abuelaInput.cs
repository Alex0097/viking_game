﻿using UnityEngine;
using System.Collections;

public class abuelaInput : MonoBehaviour
{

    //=========================================================
    public float vikingoSpeed;
    public float abuelaJumpimpulso;
    public Transform grundCheckPoint;
    public LayerMask whatisGround;
    public bool ImInGround;


    private Rigidbody2D vikingobody;

    private Vector2 abuelaMovement;

    private float horImput;

    private bool vikingoJum;

    private Animator anim;

    private bool vikingoMirandoParaDondeEs;


    public Transform bulletSpawner;

    public GameObject bulletPrefap;


    bool shooting;


    //=======================================================
    void Start()
    {
        this.vikingobody = this.GetComponent<Rigidbody2D>();
        this.abuelaMovement = new Vector2();
        this.ImInGround = false;

        this.anim = GetComponent<Animator>();

        this.vikingoMirandoParaDondeEs = true;
    }
    //====================================================

    void Update()
    {

        PlayerShooting();

        this.horImput = Input.GetAxis("Horizontal");
        this.vikingoJum = Input.GetKey(KeyCode.Space);


        if ((this.horImput < 0) && (this.vikingoMirandoParaDondeEs))
        {
            this.Flip();
            this.vikingoMirandoParaDondeEs = false;

        }
        else if ((this.horImput > 0) && (!this.vikingoMirandoParaDondeEs))
        {
            this.Flip();
            this.vikingoMirandoParaDondeEs = true;

        }



        this.anim.SetFloat("VerSpeed", Mathf.Abs(this.vikingobody.velocity.y));

        if (Physics2D.OverlapCircle(this.grundCheckPoint.position, 0.10f, this.whatisGround))
        {

            this.ImInGround = true;

        }
        else
        {
            this.ImInGround = false;

        }

    }
    //========================================================
    void FixedUpdate()
    {
        this.abuelaMovement = this.vikingobody.velocity;

        this.abuelaMovement.x = horImput * vikingoSpeed;
        if (this.vikingoJum && this.ImInGround)
        {
            this.abuelaMovement.y = abuelaJumpimpulso;

        }

        this.vikingobody.velocity = this.abuelaMovement;


    }
    //========================================================
    void Flip()
    {
        Vector3 scale = this.transform.localScale;
        scale.x *= (-1);
        this.transform.localScale = scale;

    }
    //========================================================
    public void OnGetKill()
    {
        GameMaster.current.GameOver();

    }

    //========================================================



    public void PlayerShooting()
    {

        if (Input.GetButtonDown("Fire1"))
        {

            Instantiate(bulletPrefap, bulletSpawner.position, bulletSpawner.rotation);


        }

    }

}
