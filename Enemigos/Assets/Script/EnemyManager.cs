﻿using UnityEngine;
using System.Collections;

public class EnemyManager : MonoBehaviour
{



    private Animator eAnim;

    public float animDelay;

    public int enemyHealth;

    public static bool enemyDead = false;

    void Start()
    {

        eAnim = GetComponent<Animator>();

    }




    public void OnTriggerEnter2D(Collider2D col)
    {

        if (col.tag == "Bullet" )
        {
            enemyHealth -= BulletMovement.damage;
            

            if (enemyHealth <= 0)
            {
                enemyDead = true;
                eAnim.SetBool("IsDead", true);
                Destroy(gameObject, animDelay);

            }

            
        }

    }

}

